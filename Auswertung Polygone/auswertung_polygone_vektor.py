import os
import pandas as pd
from tqdm import tqdm
from osgeo import ogr

# Ordnerpfade definieren
polygon_folder = r"C:\Users\sever\OneDrive - ETH Zurich\Desktop\Bachelorarbeit\Daten\aa Entlebuch Layers\classify_results\Entlebuch_5km"
vector_folder = r"C:\Users\sever\OneDrive - ETH Zurich\Desktop\Bachelorarbeit\Daten\tlm_auswertung_zugeschnitten"

# Funktion zum Entfernen der Z-Koordinaten aus den Geometrien
def remove_z_coordinates(geometry):
    if geometry.GetGeometryName() == 'MULTIPOLYGON':
        return geometry.ConvexHull()
    else:
        return geometry

# Liste zum Sammeln der Ergebnisse initialisieren
results = []

try:
    # Initialize tqdm progress bar for polygons
    polygon_files = [file for file in os.listdir(polygon_folder) if file.endswith(".gpkg")]
    polygon_progress_bar = tqdm(polygon_files, desc="Processing polygons")
    
    # Schleife über jedes Polygon
    for polygon_file in polygon_progress_bar:
        # Polygon laden
        polygon_path = os.path.join(polygon_folder, polygon_file)
        
        driver = ogr.GetDriverByName("GPKG")
        polygons = driver.Open(polygon_path, 0)
        
        if polygons is None:
            print(f"Could not open {polygon_path}")
            continue
        
        polygons_layer = polygons.GetLayer()
        
        # Initialize dictionaries to store raster and vector statistics
        vector_objects_dict = {}
        
        # Schleife über Vektordaten
        for vector_file in os.listdir(vector_folder):
            if vector_file.endswith(".gpkg"):
                vector_path = os.path.join(vector_folder, vector_file)
                vector_name = os.path.splitext(vector_file)[0]
                
                try:
                    # Vektordaten laden - Move this line outside of the polygon loop
                    vectors = driver.Open(vector_path, 0)
                    
                    if vectors is None:
                        print(f"Could not open {vector_path}")
                        continue
                    
                    vectors_layer = vectors.GetLayer()
                    
                    # Geometrien bereinigen (Z-Koordinaten entfernen)
                    vectors_layer = vectors.GetLayer()
                    vectors_layer_defn = vectors_layer.GetLayerDefn()
                    
                    # Loop over polygons
                    for polygon_feat in polygons_layer:
                        polygon_geom = polygon_feat.GetGeometryRef()
                        polygon_name = os.path.splitext(polygon_file)[0]  # Extract the name part without the extension
                        
                        # Objekte zählen, die sich mit dem Polygon überlappen
                        overlapped_objects = []
                        vectors_layer.ResetReading()
                        for vector_feat in vectors_layer:
                            vector_geom = vector_feat.GetGeometryRef()
                            if vector_geom.Intersects(polygon_geom):
                                overlapped_objects.append(vector_feat)
                        
                        num_objects = len(overlapped_objects)
                        
                        # Ergebnisse hinzufügen
                        vector_objects_dict[f'{vector_name}_objects'] = num_objects
                except Exception as e:
                    print(f"An error occurred while processing vector file {vector_file}: {str(e)}")
                    continue
        
        # Ergebnisse für das aktuelle Polygon sammeln
        results.append(vector_objects_dict)

    # Ergebnisse in ein DataFrame konvertieren und den Namen des Polygons als erste Spalte hinzufügen
    results_df = pd.DataFrame(results)
    results_df.insert(0, 'Polygon_Name', [os.path.splitext(poly)[0] for poly in polygon_files])

    # Ergebnisse speichern
    output_file = r'C:\Users\sever\OneDrive - ETH Zurich\Desktop\Bachelorarbeit\Daten\tlm_auswertung_zugeschnitten\polygon_analysis_results_swisstlm3d.csv'
    results_df.to_csv(output_file, index=False)

except Exception as e:
    print(f"An error occurred: {str(e)}")
