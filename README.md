# BA_24

In diesem Ordner befinden sich die verwendeten Python-Skripte sowie die Ausgaben der Netzdiagrammauswertung der Bachelorarbeit "Entwicklung einer Vorgehensweise zur Identifikation von Standorten für ein Fotomonitoring in der UNESCO Biosphäre Entlebuch". 

Auswertung Polygone: Skripts um raumlich Auszuwerten, was in welchem Landschaftssegment zu finden ist. 

Clustering Algorithmus: Skript zur Landschaftssegmentierung 

Polygonauswerung: Netzdiagramm Output 

tiff combine: Dafür da mehrere TIFF Dateien zu einer zu kombinieren 


